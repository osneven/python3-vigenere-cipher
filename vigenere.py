#!/usr/bin/python3
'''
	python3-vigenere-cipher
    Copyright (C) 2017  Oliver Stochholm Neven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from sys import exit

lowercase_alphabet_sequence = "abcdefghijklmnopqrstuvwxyz"
uppercase_alphabet_sequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
number_sequence 			= "0123456789"

##### Vigenére table/square generation

# Generaes a vignere table from a sequence of characters
# If the sequence used is the lowercase latin alphabet, the table will look like this:
#	  | a b c d ..
#   - + -------
#	a | a b c d ..
#	b | b c d e ..
#	c | c d e f ..
#	d | d e f g ..
#   :   : : :
#
# The return value is a "2 dimensional" dictionary (dictionary conatining dictionaries).
# If the above sequence example is passed, the following dictionary "format" is returned:
#	{
# 		a: 	{ a: a, b: b, c: c, ... },
# 		b:	{ a: b, b: c, c: d, ... },
# 		c:	{ a: c, b: d, c: e, ... },
# 		...
#	}
#
# In this format, encrypting a charater is simple:
#	c = T[k][p] where 'c' is the encryption output, 'p' is the encryption input, 'T' is the Vigenére table and 'k' is the encryption key letter.
#
# Decryption would involve an inverse dictionary lookup, in the dictionary return of:
#	T[k] using the same variables as before.
#
def generate_vigenere_table(sequence):
	shifted_sequence = sequence
	table = {}

	# Create the table
	for a in sequence:
		sub_table = {}

		# Create the sub table
		for i,b in enumerate(sequence):

			# Add the shifted element, to the originals place
			sub_table[b] = shifted_sequence[i]

		# Add the sub table as a new colunm in the table
		table[a] = sub_table

		# Shift the sequence for the next trun
		shifted_sequence = shifted_sequence[1:] + shifted_sequence[0]

	# Return the table
	return table

##### Vigenére encryption/decryption of a single charater

# Encrypts the given plainchar (single plaintext character) using the given Vigenére table with the given keychar (single keyword character)
def encrypt_character(table, plainchar, keychar):
	return table[keychar][plainchar]

# Decrypts the given cipherchar (single ciphertext character) using the given Vigenére table with the given keychar (single keyword character)
def decrypt_character(table, cipherchar, keychar):
	sub_table = table[keychar]
	for sub_key in sub_table:
		if sub_table[sub_key] == cipherchar:
			return sub_key
	raise ValueError("Ciphertext not contained in the table.")

##### Vignére encryption/decryption of a text

# Performs either the encryption or decryption function (whatever parsed) on every character in the input string
# NOTE: Ignores any character not contained in the table
# NOTE: If inore_case is True, every all letters in both in input string and keyword is handled as uppercase (the table is not changed)
def crypt(function, table, in_string, keyword, ignore_case=True):
	out_string = ""
	keyword_len = len(keyword)

	# If ignore case is True, capitalize all letters in the input string and keyword
	if ignore_case:
		in_string = in_string.upper()
		keyword = keyword.upper()

	# Iterate through each character in the input string, and perform the function on it (either encrypt, or decrypt)
	# Ignores any character not contained in the table
	keyword_index = 0
	for in_char in in_string:

		# Perform function on the current character and shift keyword index
		try:
			out_string += function(table, in_char, keyword[keyword_index % keyword_len])
			keyword_index += 1

		# If character is not contained in the table, print a message and just append it - without performing any function - to the output string
		# NOTE: Message is diabled by default.
		except:
			out_string += in_char
			#print("Unrecognized character: '{}' - Left unchanged and in the same position as received.".format(in_char))

	# Return output string
	return out_string

# Wrapper for 'crypt()' with the 'encrypt_character()' as the function argument
# Encrypts the given plaintext using the given vigenére table with the given keyword
def encrypt(table, plaintext, keyword):
	return crypt(encrypt_character, table, plaintext, keyword)

# Wrapper for 'crypt()' with the 'decrypt_character()' as the function argument
# Decrypts the given ciphertext using the given Vigenére table with the given keyword
def decrypt(table, ciphertext, keyword):
	return crypt(decrypt_character, table, ciphertext, keyword)

##### CLI parsing

# Returns a human readable string of the given Vigenére table in the order of the fiven sequence
# For instance, if the latin alphabet was used as the sequence, the table would print in alphabetical order (Like the example in the top of this file)
def string_of_table(table, sequence):
	out_string = ""
	for plain in sequence:
		sub_table = table[plain]
		for key in sequence:
			out_string += sub_table[key] + " "
		out_string += "\n"
	return out_string

def parse_args():
	import argparse
	parser = argparse.ArgumentParser(description="Vigenére cipher tool. Does both encryption and decryption, works with both files and stdout.")

	group0 = parser.add_mutually_exclusive_group()
	group0.add_argument("-k", "--keyword",		dest="KEYWORD",			type=str,	help="The keyword used to either encrypt or decrypt.")
	group0.add_argument("-K", "--keyfile",		dest="KEYFILE",			type=str,	help="The path to the keyfile used to either encrypt or decrypt.")

	group1 = parser.add_mutually_exclusive_group()
	group1.add_argument("-i", "--input",		dest="INPUT",			type=str,	help="The input to either encrypt or decrypt.")
	group1.add_argument("-I", "--infile",		dest="INFILE",			type=str,	help="The path to the file to either encrypt or decrypt.")
	parser.add_argument("-o", "--outfile",		dest="OUTFILE",			type=str,	help="The path to the file to write the output to. If not specified, output is printed to the screen.")

	group2 = parser.add_mutually_exclusive_group()
	group2.add_argument("-e", "--encrypt",		action="store_true",				help="Toggles the encryption function of the input.")
	group2.add_argument("-d", "--decrypt",		action="store_true",				help="Toggles the decryption function of the input.")

	parser.add_argument("-p", "--print",		action="store_true",				help="Prints the Vigenére table from the given sequence.")
	group3 = parser.add_mutually_exclusive_group()
	group3.add_argument("-s", "--sequence", 	dest="SEQUENCE", 		type=str,	help="The sequence used to generate the Vigenére table. Default is the latin alphabet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'")
	group3.add_argument("-S", "--seqfile",		dest="SEQFILE",			type=str,	help="The path to the file to use as the Vigenére table sequence. Default is the latin alphabet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'")

	return parser.parse_args()
args = parse_args()

# Handle table and sequence
sequence = args.SEQUENCE
if args.SEQFILE is not None:
	with open(args.SEQFILE, "r") as f:
		sequence = f.read()
		f.close()
if sequence is None:
	sequence = uppercase_alphabet_sequence
table = generate_vigenere_table(sequence)

# Handle optional print of table
if args.print:
	print(string_of_table(table))
	exit(0)

# Handle input string
in_string = args.INPUT
if args.INFILE is not None:
	with open(args.INFILE, "r") as f:
		in_string = f.read()
		f.close()
if in_string is None:
	print("No form of input is given. Give either an input string (-i) or input file (-I).")
	exit(1)

# Handle keyword
keyword = args.KEYWORD
if args.KEYFILE is not None:
	with open(args.KEYFILE, "r") as f:
		keyword = f.read()
		f.close()
if keyword is None:
	print("No form of key material is given. Give either a keyword (-k) or keyfile (-I).")
	exit(2)

# Handle function (encryption/decryption)
out_string = None
if args.encrypt:
	out_string = encrypt(table, in_string, keyword)
elif args.decrypt:
	out_string = decrypt(table, in_string, keyword)
if out_string is not None:
	if args.OUTFILE is not None:
		with open(args.OUTFILE, "w") as f:
			f.write(out_string)
			f.close()
	else:
		print(out_string)
	exit(0)
else:
	print("No form of function is specified. For encryption do (-e) for decryption do (-d).")
	exit(3)
