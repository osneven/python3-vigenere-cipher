Python3 command line tool for the Vigenére cipher.

Supports:
- Encryption & decryption.
- Keyword, input, ouput and table sequence can all be read/written from/to a file or the CLI.
- Custom Vigenére table/square generation from a sequence.

For basic usage run: `python3 vigenere.py --help`